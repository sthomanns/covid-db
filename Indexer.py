import os
import pandas as pd 
import time 
from datetime import datetime

def __tmpl__(table_name, path): 
  TMPL = f"psql -c \"\copy {table_name} " \
            f"FROM '{path}' " \
            f"delimiter ',' csv header;\" covid postgres"
  return TMPL 

class Indexer:
  def __init__(self, key, *props):
    self.key = key
    self.props = props 

  def extract(self, input_path): 
    self.dim = pd.read_csv(input_path)
    self.dim = self.dim[[self.key] + [a for a in self.props]]

  def export(self, output_path, table_name): 
    self.dim.to_csv(output_path)
    os.system(__tmpl__(table_name, output_path))
   