import pugsql
import pandas as pd 
import matplotlib.pyplot as plt
import seaborn as sns 
import os
import time 
from datetime import datetime
from Indexer import Indexer

def index_data(): 
  # FROM: Social Determinants of Health Data Resources, CDC Gis Resources, Food Environment Atlas (USDA)
  #   https://github.com/nytimes/covid-19-data
  #   https://www.cdc.gov/dhdsp/maps/gisx/resources/geo-spatial-data.html
  #   https://www.ers.usda.gov/data-products/food-environment-atlas/about-the-atlas.aspx
  #   https://www.data.gov/climate/foodresilience/foodresilience-tools
  #   http://waterdata.usgs.gov/nwis
  #   http://www.sdss.jhu.edu/~szalay/class/2020/covid19/covid19.htm
  #   https://github.com/JieYingWu/COVID-19_US_County-level_Summaries
  #   https://www.safegraph.com/
  #
  # df_counties = pd.read_csv('us-counties.csv')

  # Obesity and diabetes dimension (i.e. do counties with higher obesity/diabetes rates generally have higher death rates?)
  healthIndexer = Indexer('FIPS','PCT_OBESE_ADULTS13','PCT_DIABETES_ADULTS13')
  healthIndexer.extract('data/FoodEnvAtlas/HEALTH-Table 1.csv')
  healthIndexer.export('data/data_obesity.csv', 'dm_obesity')

  # Socio-economic dimension (i.e. do counties with higher median income generally have lower death rates?)
  socioeconIndexer = Indexer('FIPS','POVRATE15','PCT_65OLDER10','MEDHHINC15','PCT_NHBLACK10')
  socioeconIndexer.extract('data/FoodEnvAtlas/SOCIOECONOMIC-Table 1.csv')
  socioeconIndexer.export('data/data_socioecon.csv', 'dm_socioecon')

  # Mobility dimension, Google mobility reports (i.e. changes in workplace attendance, grocery store, etc)
  googMobility = Indexer('date','country_region','sub_region_1','sub_region_2', 'workplaces_percent_change_from_baseline')
  googMobility.extract('data/GoogleMobility/Global_Mobility_Report.csv')
  googMobility.export('data/data_mobility.csv', 'mobility_facts')

  # Unique counties 
  countiesIndexer = Indexer('FIPStxt','State','County_name')
  countiesIndexer.extract('data/us-counties-ers.csv')
  countiesIndexer.export('data/data_county.csv', 'dm_county')

  # Covid data NY times
  factIndexer = Indexer('date','fips','cases','deaths')
  factIndexer.extract('data/us-counties.csv')
  factIndexer.export('data/data_facts.csv', 'covid_facts')


if __name__ == "__main__": 
  index_data()
  '''
  queries = pugsql.module('queries/')
  queries.connect('postgresql://postgres@localhost/covid')
  top_two = queries.top_two() 
  for i in top_two: 
    print(i)
  '''  
