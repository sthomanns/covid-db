import numpy as np
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import matplotlib.ticker as ticker
import pugsql
import pandas as pd 
import seaborn as sns 
from datetime import datetime
from matplotlib.ticker import MaxNLocator

def plot(): 

  queries = pugsql.module('queries/')
  queries.connect('postgresql://postgres@localhost/covid')
  obesity = queries.obesity_dimension()
  obesity_df = pd.DataFrame(data=obesity, columns=['pct_diabetes', 'pct_obesity','rate'])
  obesity_df = obesity_df.dropna()

  color_b = (0.424, 0.153, 0.090)
  color_a = (0.255, 0.627, 0.943)#43)

  fig = plt.figure(figsize=(8, 4), frameon=False, facecolor='white')
  ax1 = fig.add_subplot(111)
  ax1.set_axisbelow(True)
  ax1.yaxis.grid(True, color='0.65', ls='-', lw=1.5, zorder=0)
 
  x1= obesity_df['pct_diabetes']
  y1= obesity_df['rate']
  sns.barplot(x=x1, y=y1, color=color_a, ci=None)
  ax1.spines['left'].set_visible(False)
  ax1.spines['right'].set_visible(False)
  ax1.spines['top'].set_visible(False)
  ax1.spines['bottom'].set_visible(False)
  ax1.xaxis.set_ticks_position('bottom')
  ax1.set_xlabel('diabetes incidence')
  ax1.set_ylabel('deaths per 100 infected')

  ax1.tick_params(axis='y', length=0)
  for tick_label in ax1.yaxis.get_ticklabels():
      tick_label.set_fontsize(12)
      tick_label.set_color(color_a)
  for tick in ax1.xaxis.get_major_ticks():
      tick.label.set_fontsize(11)
      tick.label.set_horizontalalignment('center')
  ax1.xaxis.set_major_locator(MaxNLocator(10, prune='both'))

  ax1.tick_params(direction='in', axis='x', length=7, color='0.1', )
  ax1.annotate(u'fatality rate vs diabetes incidence', xy=(0.6, 0.9),
              xycoords='figure fraction', size=15, color=color_a,
              fontstyle='italic')
  plt.show()