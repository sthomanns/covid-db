-- :name top_two
;with cte as (
	select 
		max(cases) as cases, dm.county_name, dm.county_state,
			row_number() over (
				order by max(cases) desc
			) as row_num
	from 
		covid_facts f
	join 
		dm_county dm on dm.county_fips = f.fips
	group by 
		dm.county_name, dm.county_state
	having 
		max(cases) > 0
)
select 
	(select county_name from cte where row_num = 1 limit 1) as worst_case, 
	(select county_name from cte where row_num = 2 limit 1) as second_worst_case 