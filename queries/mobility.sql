-- :name mobility_dimension TODO
/*
;with cte_aggfips as (
	select 
		d.county_name, max(c.cases) as total_cases, max(c.deaths) as total_deaths
	from 
		covid_facts c 
	join 
		dm_county d on d.county_fips = c.fips
	group by 
		d.county_name
  having max(c.cases) >= 100 and max(c.deaths) >= 1
)
select 
  ntile(10) 
		over (order by o.county_min_workplace) as pct_workplace,
			(cast(a.total_deaths as float) / 
												a.total_cases) * 100 as rate
from cte_aggfips a 
join ( 
		select sub_region_1, sub_region_2, 
							min(workplaces_percent_change_from_baseline) as county_min_workplace
			from mobility_facts
			where 
				country_region = 'United States' 
			and 
				workplaces_percent_change_from_baseline is not null 
			group by sub_region_1, sub_region_2
			order by min(workplaces_percent_change_from_baseline) desc 
) x 
	on x.sub_region_2 = a.county_name
	*/