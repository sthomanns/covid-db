-- :name socioecon_dimension
;with cte_aggfips as (
	select 
		fips, max(cases) as total_cases, max(deaths) as total_deaths
	from 
		covid_facts
	group by 
		fips
  having max(cases) >= 100 and max(deaths) >= 1
)
select 
	round(o.county_medhhinc15/1000) as pct_medhhinc15, 
	ntile(10) 
		over (order by o.county_povrate15) as pct_povrate15,
  ntile(10) 
		over (order by o.county_nhblack10) as pct_nhblack10,
  (
			cast(a.total_deaths as float) / 
														a.total_cases) * 100 as rate
from cte_aggfips a 
join dm_socioecon o on o.county_fips = a.fips