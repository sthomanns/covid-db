-- :name obesity_dimension
;with cte_aggfips as (
	select 
		fips, max(cases) as total_cases, max(deaths) as total_deaths -- TODO verify max() vs sum() in base data 
	from 
		covid_facts
	group by 
		fips
  having max(cases) >= 100 and max(deaths) >= 1
)
select 
  ntile(10) over (order by o.county_pct_diabetes_adults13) as pct_diabetes, 
  ntile(10) over (order by o.county_pct_obese_adults13) as pct_obesity,
    cast(a.total_deaths as float) / a.total_cases as rate
from cte_aggfips a 
join dm_obesity o on o.county_fips = a.fips
join dm_county c on c.county_fips = a.fips