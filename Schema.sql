
-- County is the dimension from which you can build new dimensions, but in star schema still have this separate 
create table dm_county(
	id serial primary key not null, 
  	county_fips int not null, 
  	county_state varchar(16) not null, 
  	county_name  varchar(32) not null
)
-- Obesity dimension
create table dm_obesity(
	id int primary key not null, 
	county_fips int not null unique, 
	county_pct_obese_adults13 float null,
	county_pct_diabetes_adults13 float null
)
-- Socio-economic dimension
create table dm_socioecon(
	id int primary key not null, 
	county_fips int not null unique, 
	county_povrate15 float null,
	county_65older10 float null,
	county_medhhinc15 float null,
	county_nhblack10 float null
)
-- Mobility data from Google
create table mobility_facts(
	id int primary key not null, 
	"date" timestamp not null, 
	country_region varchar(32) not null, 
	sub_region_1 varchar(128) null, 
	sub_region_2 varchar(128) null,
	workplaces_percent_change_from_baseline float null
)
-- Cases and deaths (same granularity) 
create table covid_facts(
	id serial primary key not null, 
	"date" date not null,
	fips float null, 
	cases int not null, 
	deaths int not null 
)