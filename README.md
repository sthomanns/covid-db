A work-in-progress data model with facts on covid-19 at the US county level, as well as socio-economic and other social determinants of health dimensions for analysis. The schema allows for visualizations like the following, which shows the case fatality rate aggregated across California counties grouped into percentiles of median household income (higher x-axis value represents greater median income):
<br />
<br />
<img src="https://gitlab.com/sthomanns/covid-db/-/raw/master/assets/plot.png" width="680" height="400">
<br/>
Or, this plot which shows the death rate against the incidence of diabetes across communities, 
<br />
<br />
<img src="https://gitlab.com/sthomanns/covid-db/-/raw/master/assets/diabetes.png" width="680" height="400">